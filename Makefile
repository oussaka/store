init: init-submodules start install

install:
	git submodule foreach 'make install'

start:
	docker network create store || true
	git submodule foreach 'make start'
	docker-compose up -d

stop:
	git submodule foreach 'make stop'
	docker-compose down

destroy:
	make stop
	git submodule foreach 'make destroy'
	docker network rm store || true
	docker-compose rm

init-submodules:
	git submodule init
	git submodule update --recursive --init
	git submodule foreach 'git checkout master'